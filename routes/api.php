<?php

use Illuminate\Http\Request;

/*
Kullanıcı giriş ve kayıt requestleri
*/
Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

/*

Jwt authentication ile api requestleri

*/
Route::group(['middleware' => 'auth'], function() {
	Route::get('logout', 'ApiController@logout'); 
	Route::get('user', 'ApiController@getAuthUser');
	 
	Route::get('todos','TodosController@list');
	Route::post('todos','TodosController@create');
	Route::patch('todos/{id}','TodosController@update')->where('id', '[0-9]+');
	Route::delete('todos/{id}','TodosController@delete')->where('id', '[0-9]+');
	Route::get('todos/{id}','TodosController@read')->where('id', '[0-9]+');
});