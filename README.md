#JWT Auth ve Laravel Framework 5.8.29 ile Rest Api

##Jwt auth kullanarak Rest Api geliştirmesi,

* Yüklemek için repositoryi klonlayın ve aşağıdaki komutu koşunuz.

* `composer update`

* .env-example dosyasının içeriklerini .env adlı bir dosya oluşturup kopyalayınız.

* Database ayarlarını .env dosyasından yapabilirsiniz.

* Database'i oluşturmak için ` php artisan make migrate ` komutunu koşunuz.

* JWT nin anahtarını oluşturmak için `php artisan jwt:secret` komutunu koşunuz.

* Çalıştırmak için `php artisan serve` komudunu koşunuz.

#Apinin Test Edilmesi

Methodların çalıştırılması için Postman programı kullanılmıştır. Postman https://www.getpostman.com/

Kayıt olma testi

Post methodu kullanılarak Post http://127.0.0.1:8000/api/register parametreler email ve password.

Diğer örnek linkleri api.php dosyasından bakılabilir.
