<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterAuthRequest;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{

    /**
     * Kullanıcı kaydının oluşturulması
     */

 public function register(RegisterAuthRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password =Hash::make($request->password);
        $user->save();
 
        return response()->json(['success' => true,'data' => $user], 200);
    }

    /**
     * Kullanıcı girişi
     */

    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        
        
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }


    /**
     * Kullanıcı çıkış
     
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Başarıyla çıkış yapıldı']);
    }


    /**
     * Tokenın oluşturulması
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function guard()
    {
        return Auth::guard();
    }
}
