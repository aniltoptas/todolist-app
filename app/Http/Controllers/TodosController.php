<?php

namespace App\Http\Controllers;

use \App\Http\Requests\CreateTodoRequest;
use \App\Http\Requests\ReadTodoRequest;
use \App\Http\Requests\UpdateTodoRequest;
use \App\Http\Requests\DeleteTodoRequest;
use Illuminate\Http\Request;
use \App\Todo;
use JWTAuth;

class TodosController extends Controller
{

	protected $user;
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

     /*
     * Todo listesinin çıktısı
     */
    public function list()
	{
		$todos=Todo::all();

		$message=[];

		$message["code"]=200;
		$message["result"]=$todos;

    	return $message;
	}

     /*
     * Todo oluşturulması
     */
	public function create(CreateTodoRequest $request)
	{
		Todo::create($request->all());

		$message=[];

		$message["code"]=200;
		$message["result"]='İşlem başarılı.';

    	return $message;
	}	

	/*
     * Todo güncellemesi
     */
	public function update(UpdateTodoRequest $request,$id )
	{
		Todo::where('id',$id)->update($request->all());

		$message=[];

		$message["code"]=200;
		$message["result"]='Güncelleme yapıldı.';

    	return $message;

	}

	 /*
     * Todo silinmesi
     */
	public function delete(DeleteTodoRequest $request,$id)
	{
		Todo::where('id',$id)->delete($request->all());
		$message=[];

		$message["code"]=200;
		$message["result"]='Kayit silindi.';

    	return $message;

	}

	/*
     * Todo listesinin belirtilen id'ye göre çıktısı
     */
	public function read($id)
	{
		$result=Todo::where('id',$id)->find($id);
		$message=[];

		$message["code"]=200;
		$message["result"]='Kayit getirildi.';

		return $result;
	}

}
