<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterAuthRequest extends FormRequest
{
    /**
     * Kullanıcının requesti yapabilmesi için authentication tanımı
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Uygulanacak requeste göre yapılacak validationlar.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name' => 'required|string',
              'email' => 'required|email|unique:users',
                'password' => 'required|string|min:6|max:10'
        ];
    }
}
